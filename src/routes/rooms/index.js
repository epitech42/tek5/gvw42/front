import { h } from 'preact';
import { useState, useEffect } from 'preact/hooks';

import style from './style';

import axios from '../../request';

import RoomsListItem from '../../components/roomsListItem';

import Search from 'preact-icons/fa/search';

const ListRooms = () => {
    const [rooms, setRooms] = useState([]);
    const [searchInput, setSearchInput] = useState("");
    const [toSearch, setToSearch] = useState("");
    
    const filterByValue = (array, string) => {     return array.filter(o => {     	return Object.keys(o).some(k => {       	if(typeof o[k] === 'string') return o[k].toLowerCase().includes(string.toLowerCase());       });      }); }
    
    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            onSearch();
        }
    }
    
    const onSearch = () => {
	setToSearch(searchInput);
    }
    
    useEffect(() => {
	axios.get('/room/list')
	    .then((response) => {
		// handle success
		console.log(response);
		setRooms(response.data);
	    })
	    .catch((error) => {
		// handle error
		console.log(error);
	    })
    }, [false]);

    return (
	    <div class={style.listRooms}>
	    <h1>List Rooms</h1>
	    <div>Click on the room, you want to join</div>
            <div class={style.searchContainer}>
	    <input class={style.searchInput} type="text" placeholder="Search.." onInput={(e) => setSearchInput(e.target.value)} onKeyDown={handleKeyDown} />
            <button class={style.searchBtn} onClick={onSearch}><Search size={24} class={style.searchIcon} /></button>
	    </div>
	    <div class={style.list}>
	    {
		filterByValue(rooms, toSearch).map((room) => (
			<RoomsListItem  id={room._id} title={room.title} description={room.description} isPasswordProtected={room.isPasswordProtected} />
		))
	    }
	{
	    !filterByValue(rooms, toSearch).length &&
		<p>NO ROOMS TO SHOW</p>
	}
	</div>

	    
	</div>
    );
}

export default ListRooms;
