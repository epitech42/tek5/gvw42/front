import { h } from 'preact';
import style from './style';
import { useState } from 'preact/hooks';

import axios from '../../request';

import { route } from 'preact-router';

import ToggleInputContent from '../../components/ToggleInputContent';

const SignUp = () => {
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();

    const [TypeInput, setTypeInput] = useState("password");
    
    const [errors, setErrors] = useState([]);
    
    const handleUsername = (event) => {
	setUsername(event.target.value);
    }
    
    const handlePassword = (event) => {
	setPassword(event.target.value);
    }

    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            onClick();
        }
    }
    
    const onClick = () => {
	//console.log(username + password);
	if (username !== undefined && password !== undefined){
	    axios.post('/user/register', {
		username,
		password
	    })
		.then((response) => {
		    //console.log(response.data.token);
		console.log(response.data.message);
		    route('/signin');
		})
		.catch((error) => {
		    console.log(error);
		    setErrors(error.response.data.errors);
		});
	}
	else{
            setErrors([{msg: 'Please fill username and password'}])
	}
    }
    
    return (
	<div class={style.login}>
	    <h1>Sign Up</h1>
	    {
                errors.map((error) => (
			<p>{error.msg}</p>
	        ))
            }
	    <div class={style.row}>
	    <div class={style.col-25}>
	         <label>Username:</label>
	      </div>
	    <div class={style.col-75}>
	         <input type="text" name="username" value={username} onInput={handleUsername} onKeyDown={handleKeyDown} />
	      </div>
	    </div>
            <div class={style.row}>
	    <div class={style.col-25}>
	        <label>Password:</label>
	      </div>
	    <div class={style.col-75}>
	    <ToggleInputContent TypeInput={TypeInput} setTypeInput={setTypeInput}>
	    <input type={TypeInput} name="password" value={password} onInput={handlePassword} onKeyDown={handleKeyDown} />
	    </ToggleInputContent>
              </div>
	    </div>
	    <div class={style.row}>
	       <button type="submit" onClick={onClick}>Submit</button>
	    </div>
	</div>
    );
}

export default SignUp;
