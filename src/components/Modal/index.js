import { h } from 'preact';
import style from './style';

import Close from 'preact-icons/fa/close';

const Modal = ({handleClose, show, children}) => {
    const showHideClass = show ? style.displayBlock : style.displayNone;
    return (
	    <div class={`${style.modal  } ${  showHideClass}`}>
	    <section class={style.modalMain}>
	    <Close  onClick={handleClose} />
            {children}
	    </section>
	    </div>
    );
}

export default Modal;
