import { h } from 'preact';
import { useState } from 'preact/hooks';
import { Link } from 'preact-router/match';
import style from './style.css';

import Modal from '../Modal';

//import Edit from 'preact-icons/fa/edit';

import ToggleInputContent from '../ToggleInputContent';

import Hamburger from 'preact-icons/fa/bars';
import Add from 'preact-icons/fa/plus';
import Edit from 'preact-icons/fa/edit';
import Send from 'preact-icons/fa/paper-plane';

const HeaderRoom = ({autoPlay, setAutoPlay, password, setPassword, setSocketPassword, passwordAdmin, setPasswordAdmin, setSocketAdminPassword, isPasswordProtected, setIsPasswordProtected}) => {
    const [isMenuOpen, setIsMenuOpen] = useState(false);

    const [isPasswordProtectedAdmin, setIsPasswordProtectedAdmin] = useState(false);
    //const [isPassword, setIsPassword] = useState(false);
    
    const [isModalOpen, setIsModalOpen] = useState(false);

    //const [errors, setErrors] = useState([]);

    const [TypeInput, setTypeInput] = useState('password');
    const [TypeInputAdmin, setTypeInputAdmin] = useState('password');

    const [editRoomPassword, setEditRoomPassword] = useState(false);
    const [editRoomPasswordAdmin, setEditRoomPasswordAdmin] = useState(false);

    const onClickBar = () => {
	setIsMenuOpen(!isMenuOpen);
        console.log(isMenuOpen);
    }
   
    const Menu = isMenuOpen ? style.responsive : '';
    
	return (
	        <div class={`${style.header  } ${  Menu}`}>
		<h4>Admin Mode</h4>
                <nav>
		<Link activeClassName={style.active} class={style.icon} onClick={onClickBar}><Hamburger /></Link>

		<Link onClick={() => setAutoPlay(!autoPlay)}>
		<label class={style.switch} >
		<input type="checkbox" checked={autoPlay} onClick={() => setAutoPlay(!autoPlay)} />
		<span class={`${style.slider  } ${  style.round}`} />
		</label>
		<label class={style.labelSwitch}>AutoPlay</label>
	        </Link>

		<Link onClick={() => setIsPasswordProtected(!isPasswordProtected)}>
		<label class={style.switch} >
		<input type="checkbox" checked={isPasswordProtected} onClick={() => { setIsPasswordProtected(!isPasswordProtected);}} />
		<span class={`${style.slider  } ${  style.round}`} />
		</label>
		<label class={style.labelSwitch}>Room Password</label>
	        </Link>
	    
		<Link onClick={() => setIsModalOpen(true)}>
		<Add />
		</Link>

	        <Modal
            show={isModalOpen}
            handleClose={() => setIsModalOpen(false)}>
		<h4 style={{color: "black"}}>Edit Room</h4>
	        <div class={style.EditModal}>
	    
		<div>
		<label class={style.switch} onClick={() => setAutoPlay(!autoPlay)}>
		<input type="checkbox" checked={autoPlay} onClick={() => setAutoPlay(!autoPlay)} />
		<span class={`${style.slider  } ${  style.round}`} />
		</label>
		<label class={`${style.labelSwitch  } ${  style.noIcon}`}>AutoPlay</label>
	        </div>
	    
	    	<div>
		<label class={style.switch} onClick={() => setIsPasswordProtected(!isPasswordProtected)}>
		<input type="checkbox" checked={isPasswordProtected} onClick={() => { setIsPasswordProtected(!isPasswordProtected);}} />
		<span class={`${style.slider  } ${  style.round}`} />
		</label>
		<Edit size={24} onClick={() => setEditRoomPassword(!editRoomPassword)} />
		{!editRoomPassword &&
		 <label class={style.labelSwitch}>Room Password</label>
		}
		{editRoomPassword &&
		 <div class={style.inlineInput}>
		 <div class={`${style.col-75  } ${  style.inlineInput}`}>
		 <ToggleInputContent TypeInput={TypeInput} setTypeInput={setTypeInput}>
		 <input type={TypeInput} name="password" value={password} placeholder="Room Password" onInput={(e) => setPassword(e.target.value)} />
		 </ToggleInputContent>
		 <Send size={24} onClick={setSocketPassword} />
		 </div>
		 </div>
		}
	        </div>
		
		<div>
		<label class={style.switch} onClick={() => setIsPasswordAdmin(!isPasswordProtectedAdmin)}>
		<input type="checkbox" checked={isPasswordProtectedAdmin} onClick={() => { setIsPasswordAdmin(!isPasswordProtectedAdmin);}} />
		<span class={`${style.slider  } ${  style.round}`} />
		</label>
		<Edit size={24} onClick={() => setEditRoomPasswordAdmin(!editRoomPasswordAdmin)} />
		{!editRoomPasswordAdmin &&
		 <label class={style.labelSwitch}>Room Admin Password</label>
		}
		{editRoomPasswordAdmin &&
		 <div class={style.inlineInput}>
		 <div class={`${style.col-75  } ${  style.inlineInput}`}>
		 <ToggleInputContent TypeInput={TypeInputAdmin} setTypeInput={setTypeInputAdmin}>
		 <input type={TypeInputAdmin} name="passwordAdmin" value={passwordAdmin} placeholder="Room Admin Password" onInput={(e) => setPasswordAdmin(e.target.value)} />
		 </ToggleInputContent>
		 <Send size={24} onClick={setSocketAdminPassword} />
		 </div>
		 </div>
		}
	    </div>
	    </div>

	    </Modal>
	    
	        </nav>
                </div>
	);
}

export default HeaderRoom;
