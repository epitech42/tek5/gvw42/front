import { h } from 'preact';
import style from './style';
import { useState, useEffect } from 'preact/hooks';

import VideoRoomItem from '../../components/videoRoomItem';

import Add from 'preact-icons/fa/plus-circle';

import Modal from '../Modal';

const ListVideos = ({playlist, setPlaylist, addVideo, deleteVideo, isAdmin, moveUp, moveDown}) => {
    const [showModal, setShowModal] = useState(false);
    const [videoURL, setVideoURL] = useState();

    const handleVideoURL = (e) => {
	setVideoURL(e.target.value);
    }

    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            handleSubmit();
        }
    }    
    
    const showModalAddVideo = () => {
	setShowModal(true);
    }

    const handleSubmit = () => {
	addVideo(videoURL);
	setVideoURL('');
	hideModal('');
    }

    const hideModal = () => {
        setShowModal(false);
    }


    /*useEffect(() => {
	
    }, [playlist]);*/
    useEffect(() => {
	console.log("ok");
    }, [playlist]);
    
    return (
	    <div class={style.listVideos}>
            <h4 class={style.title}><b>Playlist </b>{isAdmin &&
		    <Add size={24} onClick={showModalAddVideo} />
	    }</h4>
	    <Modal
	show={showModal}
	handleClose={hideModal}>
            <h4>Add Video</h4>
            <p>Enter Video URL</p>
          <div>
            <input type="text" name="videoUrl" value={videoURL} onInput={handleVideoURL} onKeyDown={handleKeyDown} />
            <button type="submit" onClick={handleSubmit}>Submit</button>
          </div>
            </Modal>
	    <div class={style.wrapper}>
            {
		playlist.map((video, i) => {
		    return (
			    <VideoRoomItem isAdmin={isAdmin} title={video.url} id={video._id} key={video._id} addVideo={addVideo} deleteVideo={deleteVideo} moveUp={moveUp} moveDown={moveDown} arrayOrder={i} arraySize={playlist.length} />
		    );
		})
	    }
	    </div>
	</div>
    );
}

export default ListVideos;
