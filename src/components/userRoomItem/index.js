import { h } from 'preact';
import style from './style';
import Admin from 'preact-icons/fa/user-plus';
import User from 'preact-icons/fa/user';


const userListItem = ({userID, username, isAdmin, toggleUserPerm}) => {
    const onClick = () => {
	toggleUserPerm(userID);
    }
    return (
	    <div class={style.card}>
	    <div class={style.container}>
	    <h4>
	    { isAdmin 
	      ?<Admin color={"red"}  class={style.logo} onClick={onClick}çà />
	      :<User class={style.logo} onClick={onClick} />
	    }
	    <b> {username}</b></h4>
	    </div>
	    </div>
    );
}

export default userListItem;
