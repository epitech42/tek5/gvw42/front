import { h } from 'preact';
import style from './style';

const Bubble = ({children, mine}) => {
    const align = !mine ? style.start : style.end;
    console.log(align);
    return (
	    <div class={`${style.bubble  } ${  align}`}>
	    {children}
	    </div>
    );
}

export default Bubble;
