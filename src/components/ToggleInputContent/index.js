import { h } from 'preact';
import style from './style';

import Eye from 'preact-icons/fa/eye';
import Close from 'preact-icons/fa/eye-slash';

const ToggleInputContent = ({TypeInput, setTypeInput, children}) => {
    return (
	    <div class={style.wrapper}>
	    {children}
	{TypeInput === "password"
	 ?<Eye size={24} onClick={() => setTypeInput("text")} />
	 :<Close size={24} onClick={() => setTypeInput('password')} />
	}
	</div>
    )
}

export default ToggleInputContent;
